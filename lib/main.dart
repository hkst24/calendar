import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main() {
  runApp(const MaterialApp(home: Calendar()));
}

class Calendar extends StatefulWidget {
  const Calendar({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  DateTime selectedDate = DateTime.now();
  List<String> weekDays = ['Пр', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
    backgroundColor: CupertinoColors.black,
      navigationBar: const CupertinoNavigationBar(
        backgroundColor: CupertinoColors.black,
        middle: DefaultTextStyle(
          style: TextStyle(color: CupertinoColors.white, fontSize: 16),
          child: Text('Calendar'),
        ),
      ),
      child: SafeArea(
        child: DefaultTextStyle(
          style: const TextStyle(fontSize: 16, color: CupertinoColors.white),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CupertinoButton(
                    child: const Icon(CupertinoIcons.arrow_left, color: CupertinoColors.white),
                    onPressed: () {
                      setState(() {
                        selectedDate = DateTime(selectedDate.year, selectedDate.month - 1);
                      });
                    },
                  ),
                  Text('${selectedDate.month}/${selectedDate.year}'),
                  CupertinoButton(
                    child: const Icon(CupertinoIcons.arrow_right, color: CupertinoColors.white),
                    onPressed: () {
                      setState(() {
                        selectedDate = DateTime(selectedDate.year, selectedDate.month + 1);
                      });
                    },
                  ),
                  CupertinoButton(
                    child: const Icon(CupertinoIcons.arrow_left_to_line, color: CupertinoColors.white),
                    onPressed: () {
                      setState(() {
                        selectedDate = DateTime(selectedDate.year - 1, selectedDate.month);
                      });
                    },
                  ),
                  CupertinoButton(
                    child: const Icon(CupertinoIcons.arrow_right_to_line, color: CupertinoColors.white),
                    onPressed: () {
                      setState(() {
                        selectedDate = DateTime(selectedDate.year + 1, selectedDate.month);
                      });
                    },
                  ),
                  CupertinoButton(
                    child: const Icon(CupertinoIcons.calendar_today, color: CupertinoColors.white),
                    onPressed: () {
                      setState(() {
                        selectedDate = DateTime.now();
                      });
                    },
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.7,
                child: CupertinoScrollbar(
                  child: GridView.builder(
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 7,
                      childAspectRatio: 2,
                      mainAxisSpacing: 0,
                    ),
                    itemBuilder: (context, index) {
                      int year = selectedDate.year;
                      int month = selectedDate.month;
                      int firstDayOfMonth = DateTime(year, month, 1).weekday - 1;
                      int totalDays = DateTime(year, month + 1, 0).day;
                      int previousMonthDays = DateTime(year, month, 0).day;
                      int day;
                      if (index < 7) {
                        return Container(
                          alignment: Alignment.center,
                          child: Text(weekDays[index]),
                        );
                      } else if (index >= 7 && index < firstDayOfMonth + 7) {
                        day = previousMonthDays - firstDayOfMonth + index - 6;
                        return DefaultTextStyle(
                          style: const TextStyle(fontSize: 16, color: CupertinoColors.systemGrey),
                          child: Container(
                            alignment: Alignment.center,
                            child: Text(day.toString()),
                          ),
                        );
                      } else if (index >= firstDayOfMonth + 7 && index < totalDays + firstDayOfMonth + 7) {
                        day = index - firstDayOfMonth - 6;
                        return Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: day == DateTime.now().day && month == DateTime.now().month && year == DateTime.now().year
                                ? CupertinoColors.activeBlue
                                : null,
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Text(day.toString()),
                        );
                      } else if (index < ((totalDays + firstDayOfMonth + 7 + 6) ~/ 7) * 7) {
                        day = index - totalDays - firstDayOfMonth - 6;
                        return DefaultTextStyle(
                          style: const TextStyle(fontSize: 16, color: CupertinoColors.systemGrey),
                          child: Container(
                            alignment: Alignment.center,
                            child: Text(day.toString()),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    itemCount: 49
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}